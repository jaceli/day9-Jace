# ORID



## Objective

- Today, I mainly learned an application of flyway and learned a role of flyway in database version control. At the same time, I also learned the implementation of Mapper layer. The incoming data from the client and the returned data from the background need to go through a processing of mapper layer to realize some data mapping or confidentiality.
- In the afternoon, the main group held a Retro meeting to learn about everyone's learning in the past two weeks. At the same time, we also put forward different suggestions for the future work and study.
- At the same time, we also had a presentation on the topic of cloud native, and through each group's presentation, I gained a basic understanding of the technical concepts of cloud native design.



## Reflective

- Relaxed and funny.


## Interpretative

- What I am impressed with today is the implementation of the Mapper layer of spring boot, which is relatively easy compared to the previous knowledge in the test part. At the same time, I learned a lot from the Retro and group sharing in the afternoon, and it was also very interesting to communicate with teachers and classmates.

## Decision

- I will continue to study hard in the next course and finish the homework on time.