package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<CompanyResponse> findAll() {
        List<Company> companies = jpaCompanyRepository.findAll();
        return CompanyMapper.toResponseList(companies);
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        List<Company> companies = jpaCompanyRepository.findAll(PageRequest.of(page - 1, size)).getContent();
        return CompanyMapper.toResponseList(companies);
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.toResponse(company);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company previousCompany = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (companyRequest.getName()!=null) {
            previousCompany.setName(companyRequest.getName());
        }
        jpaCompanyRepository.save(previousCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        return CompanyMapper.toResponse(jpaCompanyRepository.save(company));
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return EmployeeMapper.toResponseList(company.getEmployees());
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
